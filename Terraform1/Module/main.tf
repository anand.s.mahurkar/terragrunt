provider "aws" {
  region = "us-east-1"
}
resource "aws_instance" "example" {
  ami           = "ami-033b95fb8079dc481"
  instance_type = var.instance_type
  tags = {
    Name = var.instance_name
  }
}
