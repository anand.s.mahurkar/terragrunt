terraform {
  backend "s3" {
    bucket = "gitlab786"
    key    = "terraformstate.tf"
    region = "us-east-1"
  }
}
